﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class OperationService
    {
        public static List<Models.Operation> GetOperations()
        {
            OperationDAO o = new OperationDAO();
            return o.GetOperations().Select(c => OperationDTO.ToModelOperation(c)).ToList();
        }

        public static Models.Operation GetOperation(string operationId)
        {
            OperationDAO o = new OperationDAO();
            return OperationDTO.ToModelOperation(o.Get(int.Parse(operationId)));
        }

        public static int CreateOperation(Models.Operation operation)
        {
            CategoryDAO cd = new CategoryDAO();
            BudGestPostgreContext.Category cat = cd.GetCategoryByName(operation.category.name);
            if (cat != null)
            {
                operation.category = CategoryDTO.ToModelCategory(cat);
            }
            else
            {

                List<BudGestPostgreContext.Category> l = cd.GetCategories();
                if (l.Count == 0)
                {
                    operation.category.id = 1;
                }
                else
                {
                    operation.category.id = l.Max(x => x.Id) + 1;
                }
                cd.Create(CategoryDTO.ToDBCategory(operation.category));
            }


            OperationDAO od = new OperationDAO();
            if (operation.id <= 0)
            {
                List<BudGestPostgreContext.Operation> l = od.GetOperations();
                if (l.Count == 0)
                {
                    operation.id = 1;
                }
                else
                {
                    operation.id = l.Max(x => x.Id) + 1;
                }
            }

            od.Create(OperationDTO.ToDBOperation(operation));

            return operation.id;
        }

        public static void UpdateOperation(Models.Operation operation)
        {
            OperationDAO o = new OperationDAO();
            var op = o.Get(operation.id);
            op.Amount = operation.amount;
            op.Recipient = operation.recipient;
            op.Operationname = operation.operationName;
            op.Operationdate = DateTime.Parse(operation.operationDate);
            //Utils.CommonAction<BudGestPostgreContext.Operation>.UpdateDBObject(op, OperationDTO.ToDBOperation(operation));
            o.Update(op);
        }

        public static void DeleteOperation(string operationId)
        {
            OperationDAO o = new OperationDAO();
            o.Delete(int.Parse(operationId));
        }
    }
}
