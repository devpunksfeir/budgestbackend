﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class ProfileService
    {
        public static void UpdatePassword(Models.Credential credential)
        {
            CredentialDAO cd = new CredentialDAO();
            BudGestPostgreContext.Credential c = cd.Get(credential.id);
                if(c != null){
                    c.Password = credential.Password;
                    cd.Update(c);
                }
        }

        public static List<Models.Profile> GetProfiles()
        {
            ProfileDAO p = new ProfileDAO();
            return p.GetProfiles().Select(c => ProfileDTO.ToModelProfile(c)).ToList();
        }

        public static Models.Profile GetProfile(string nickname)
        {
            ProfileDAO p = new ProfileDAO();
            var pr = p.GetProfileByNickname(nickname);
            if (pr != null)
            {
                return ProfileDTO.ToModelProfile(p.GetProfileByNickname(nickname));
            }
            return new Models.Profile() { id = -1 };
        }

        public static Models.Profile CreateProfile(Models.Profile profile){
            Models.User user = profile.user;
            UserDAO ud = new UserDAO();
            var users = ud.GetUsers();
            if (user.id <= 0)
            {
                user.id = users.Count == 0 ? 1 : users.Max(x => x.Id) + 1;

            }

            Models.Credential c = profile.LoginInfo;
            CredentialDAO cd = new CredentialDAO();
            var credentials = cd.GetCredentials();
            if (c.id <= 0)
            {
                c.id = credentials.Count == 0 ? 1 : credentials.Max(x => x.Id) + 1;
            }

            ProfileDAO p = new ProfileDAO();
            var profiles = p.GetProfiles();
            if (profile.id <= 0)
            {
                profile.id = profiles.Count == 0 ? 1 : profiles.Max(x => x.Id) + 1;
            }

            p.Create(ProfileDTO.ToDBProfile(profile));

            return profile;
        }

        public static void UpdateProfile(Models.Profile profile)
        {
            ProfileDAO p = new ProfileDAO();
            p.Update(ProfileDTO.ToDBProfile(profile));
        }

        public static void DeleteProfile(string profileId)
        {
            ProfileDAO p = new ProfileDAO();
            p.Delete(int.Parse(profileId));
        }
    }
}