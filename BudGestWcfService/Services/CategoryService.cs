﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class CategoryService
    {
        public static List<Models.Category> GetCategories()
        {
            CategoryDAO c = new CategoryDAO();
            return c.GetCategories().Select(cd => CategoryDTO.ToModelCategory(cd)).ToList();
        }

        public static Models.Category GetCategory(string categoryId)
        {
            CategoryDAO c = new CategoryDAO();
            return CategoryDTO.ToModelCategory(c.Get(int.Parse(categoryId)));
        }

        public static void CreateCategory(Models.Category category)
        {
            CategoryDAO c = new CategoryDAO();
            c.Create(CategoryDTO.ToDBCategory(category));
        }
        public static void UpdateCategory(Models.Category category)
        {
            CategoryDAO c = new CategoryDAO();
            c.Update(CategoryDTO.ToDBCategory(category));
        }
        public static void DeleteCategory(string categoryId)
        {
            CategoryDAO c = new CategoryDAO();
            c.Delete(int.Parse(categoryId));
        }

        public static void DeleteCategoriesOperations()
        {
            OperationDAO o = new OperationDAO();
            o.DeleteAllOperations();

            CategoryDAO c = new CategoryDAO();
            c.DeleteAllCategories();
            //c.Delete(int.Parse(categoryId));
        }
    }
}
