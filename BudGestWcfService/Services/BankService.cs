﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class BankService
    {
        public static List<Models.Bank> GetBanks()
        {
            BankDAO b = new BankDAO();
            return b.GetBanks().Select(bk => BankDTO.ToModelBank(bk)).ToList();
        }

        public static Models.Bank GetBank(string name)
        {
            BankDAO b = new BankDAO();
            return BankDTO.ToModelBank(b.GetBank(name));
        }

        public static void CreateBank(Models.Bank bank)
        {
            BankDAO bd = new BankDAO();
            if (bank.id <= 0)
            {
                List<BankPostgreContext.Bank> l = bd.GetBanks();
                if (l.Count == 0)
                {
                    bank.id = 1;
                }
                else
                {
                    bank.id = l.Max(x => x.Id) + 1;
                }
                bd.Create(BankDTO.ToDBBank(bank));
            }
            else
            {
                if (bd.Get(bank.id) == null)
                {
                    bd.Create(BankDTO.ToDBBank(bank));
                }
            }
        }

        public static void UpdateBank(Models.Bank bank)
        {
            BankDAO b = new BankDAO();
            b.Update(BankDTO.ToDBBank(bank));
        }

        public static void DeleteBank(string bankId)
        {
            BankDAO b = new BankDAO();
            b.Delete(int.Parse(bankId));
        }

    }
}
