﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class BankAccountService
    {
        public static List<Models.BankAccount> GetBankAccounts()
        {
            BankAccountDAO ba = new BankAccountDAO();

            List<BankPostgreContext.Bankaccount> b = ba.GetBankAccounts();
            return b.Select(c => BankAccountDTO.ToModelBankAccount(c)).ToList();
        }

        public static Models.BankAccount GetBankAccount(string bankAccountId)
        {
            BankAccountDAO ba = new BankAccountDAO();
            return BankAccountDTO.ToModelBankAccount(ba.Get(int.Parse(bankAccountId)));
        }

        public static List<Models.BankAccount> GetBankAccountByUser(string userId)
        {
            BankAccountDAO ba = new BankAccountDAO();
            return ba.GetByUser(int.Parse(userId)).Select(c => BankAccountDTO.ToModelBankAccount(c)).ToList();
        }

        public static int CreateBankAccount(Models.BankAccount bankAccount)
        {

            UserDAO cd = new UserDAO();
            if (bankAccount.user.id <= 0)
            {
                List<BudGestPostgreContext.User> l = cd.GetUsers();
                if (l.Count == 0)
                {
                    bankAccount.user.id = 1;
                }
                else
                {
                    bankAccount.user.id = l.Max(x => x.Id) + 1;
                }
                cd.Create(UserDTO.ToDBUser(bankAccount.user));
            }
            else
            {
                if (cd.Get(bankAccount.user.id) == null)
                {
                    cd.Create(UserDTO.ToDBUser(bankAccount.user));
                }
            }

            BankDAO bd = new BankDAO();
            if (bankAccount.bank.id <= 0)
            {
                List<BankPostgreContext.Bank> l = bd.GetBanks();
                if (l.Count == 0)
                {
                    bankAccount.bank.id = 1;
                }
                else
                {
                    bankAccount.bank.id = l.Max(x => x.Id) + 1;
                }
                bd.Create(BankDTO.ToDBBank(bankAccount.bank));
            }
            else
            {
                if (bd.Get(bankAccount.bank.id) == null)
                {
                    bd.Create(BankDTO.ToDBBank(bankAccount.bank));
                }
            }

            //bank

            //bankAccount.mandatary = new List<Models.User>();
            //bankAccount.limits = new List<Models.Limit>();
            //bankAccount.operations = new List<Models.Operation>();

            BankAccountDAO ba = new BankAccountDAO();
            if (bankAccount.id <= 0)
            {
                List<BankPostgreContext.Bankaccount> l = ba.GetBankAccounts();
                if (l.Count == 0)
                {
                    bankAccount.id = 1;
                }
                else
                {
                    bankAccount.id = l.Max(x => x.Id) + 1;
                }
            }
            ba.Create(BankAccountDTO.ToDBBankAccount(bankAccount));

            ProfileAccountDAO pa = new ProfileAccountDAO();
            pa.Create(new BudGestPostgreContext.Profileaccount()
            {
                Bankaccountid = bankAccount.id,
                Profileid = bankAccount.profileId
            });

            return bankAccount.id;
        }

        public static void UpdateBankAccount(Models.BankAccount bankAccount)
        {
            BankAccountDAO ba = new BankAccountDAO();
            var account = ba.Get(bankAccount.id);
            account.Amount = bankAccount.amount;
            //Bank = BankDTO.ToDBBank(bankaccount.bank),
            //account.Bankid= bankAccount.bank.id;
            account.Devise = bankAccount.devise;
            account.Earnings = bankAccount.earnings;
            account.Ease = bankAccount.ease;
            account.Iban = bankAccount.iban;
            account.Name = bankAccount.name;
            account.Overdraft = bankAccount.overdraft;
            account.Type = bankAccount.type;

            ba.Update(BankAccountDTO.ToDBBankAccount(bankAccount));
        }

        public static void DeleteBankAccount(string bankAccountId)
        {

            BankAccountOperationDAO baod = new BankAccountOperationDAO();
            List<BudGestPostgreContext.Operation> l = baod.GetOperationByBankAccountId(int.Parse(bankAccountId));

            OperationDAO o = new OperationDAO();

            l.ForEach(x => o.Delete(x.Id));
            l.ForEach(x => baod.DeleteBankAccountOperation(int.Parse(bankAccountId), x.Id));

            ProfileAccountDAO pa = new ProfileAccountDAO();
            pa.Delete(int.Parse(bankAccountId));

            BankAccountDAO ba = new BankAccountDAO();
            ba.Delete(int.Parse(bankAccountId));
        }

    }
}
