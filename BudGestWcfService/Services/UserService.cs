﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class UserService
    {
        public static List<Models.User> GetUsers()
        {
            UserDAO u = new UserDAO();
            return u.GetUsers().Select(c => UserDTO.ToModelUser(c)).ToList();
        }

        
        public static Models.User GetUser(string userId)
        {
            UserDAO u = new UserDAO();
            return UserDTO.ToModelUser(u.Get(int.Parse(userId)));
        }

        public static void CreateUser(Models.User user)
        {
            UserDAO u = new UserDAO();
            u.Create(UserDTO.ToDBUser(user));
        }

        public static void UpdateUser(Models.User user)
        {
            UserDAO u = new UserDAO();
            u.Update(UserDTO.ToDBUser(user));
        }

        public static void DeleteUser(string userId)
        {
            UserDAO u = new UserDAO();
            u.Delete(int.Parse(userId));
        }
    }
}
