﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class LimitService
    {
        public static List<Models.Limit> GetLimits()
        {
            LimitDAO l = new LimitDAO();
            return l.GetLimits().Select(c => LimitDTO.ToModelLimit(c)).ToList();
        }

        public static Models.Limit GetLimit(string limitId)
        {
            LimitDAO l = new LimitDAO();
            return LimitDTO.ToModelLimit(l.Get(int.Parse(limitId)));
        }

        public static void CreateLimit(Models.Limit limit)
        {
            LimitDAO l = new LimitDAO();
            l.Create(LimitDTO.ToDBLimit(limit));
        }

        public static void UpdateLimit(Models.Limit limit)
        {
            LimitDAO l = new LimitDAO();
            l.Update(LimitDTO.ToDBLimit(limit));
        }

        public static void DeleteLimit(string limitId)
        {
            LimitDAO l = new LimitDAO();
            l.Delete(int.Parse(limitId));
        }
    }
}
