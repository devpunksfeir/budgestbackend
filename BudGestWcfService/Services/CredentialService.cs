﻿using BudGestWcfService.DAO;
using BudGestWcfService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BudGestWcfService.Services
{
    class CredentialService
    {
        public static List<Models.Credential> GetCredentials()
        {
            CredentialDAO c = new CredentialDAO();
            return c.GetCredentials().Select(cd => CredentialDTO.ToModelCredential(cd)).ToList();
        }

        public static Models.Credential GetCredential(string credentialId)
        {
            CredentialDAO c = new CredentialDAO();
            return CredentialDTO.ToModelCredential(c.Get(int.Parse(credentialId)));
        }

        public static void CreateCredential(Models.Credential credential)
        {
            CredentialDAO c = new CredentialDAO();
            c.Create(CredentialDTO.ToDBCredential(credential));
        }

        public static void UpdateCredential(Models.Credential credential)
        {
            CredentialDAO c = new CredentialDAO();
            c.Update(CredentialDTO.ToDBCredential(credential));
        }

        public static void DeleteCredential(string credentialId)
        {
            CredentialDAO c = new CredentialDAO();
            c.Delete(int.Parse(credentialId));
        }
    }
}
