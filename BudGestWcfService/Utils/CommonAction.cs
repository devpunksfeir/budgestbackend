﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BudGestWcfService.Utils
{
    class CommonAction<T>
    {
        public static void UpdateDBObject(T dbObject, T updateObject)
        {
            Type typ = dbObject.GetType();
            List<PropertyInfo> pList = typ.GetProperties().ToList();
            foreach (PropertyInfo p in pList)
            {
                if (!p.Name.ToLower().Contains( "id"))
                    p.SetValue(dbObject, p.GetValue(updateObject,null),null);
            }
        }

    }
}
