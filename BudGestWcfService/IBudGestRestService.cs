﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BudGestWcfService
{
    [ServiceContract]
    public interface IBudGestRestService
    {
        [OperationContract]
        string GetData(string value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        List<BudGestWcfService.Models.Bank> GetBanks();

        [OperationContract]
        BudGestWcfService.Models.Bank GetBank(string name);

        [OperationContract]
        void CreateBank(Models.Bank bank);

        [OperationContract]
        void UpdateBank(Models.Bank bank);

        [OperationContract]
        void DeleteBank(string bankId);

        [OperationContract]
        List<Models.Profile> GetProfiles();

        [OperationContract]
        void UpdatePassword(Models.Credential credential);

        [OperationContract]
        Models.Profile GetProfile(string nickname);

        [OperationContract]
        Models.Profile CreateProfile(Models.Profile profile);

        [OperationContract]
        void UpdateProfile(Models.Profile profile);

        [OperationContract]
        void DeleteProfile(string profileId);

        [OperationContract]
        List<Models.BankAccount> GetBankAccounts();

        [OperationContract]
        Models.BankAccount GetBankAccount(string bankAccountId);

        [OperationContract]
        List<Models.BankAccount> GetBankAccountByUser(string userId);
        
        [OperationContract]
        int CreateBankAccount(Models.BankAccount bankAccount);

        [OperationContract]
        void UpdateBankAccount(Models.BankAccount bankAccount);

        [OperationContract]
        void DeleteBankAccount(string bankAccountId);

        [OperationContract]
        List<Models.Operation> GetOperations();

        [OperationContract]
        Models.Operation GetOperation(string operationId);

        [OperationContract]
        int CreateOperation(Models.Operation operation);

        [OperationContract]
        void UpdateOperation(Models.Operation operation);

        [OperationContract]
        void DeleteOperation(string operationId);

        [OperationContract]
        List<Models.Category> GetCategories();

        [OperationContract]
        Models.Category GetCategory(string categoryId);

        [OperationContract]
        void CreateCategory(Models.Category category);

        [OperationContract]
        void UpdateCategory(Models.Category category);

        [OperationContract]
        void DeleteCategory(string categoryId);

        [OperationContract]
        void DeleteCategoriesOperations();

        [OperationContract]
        List<Models.User> GetUsers();

        [OperationContract]
        Models.User GetUser(string userId);

        [OperationContract]
        void CreateUser(Models.User user);

        [OperationContract]
        void UpdateUser(Models.User user);

        [OperationContract]
        void DeleteUser(string userId);

        [OperationContract]
        List<Models.Limit> GetLimits();

        [OperationContract]
        Models.Limit GetLimit(string limitId);

        [OperationContract]
        void CreateLimit(Models.Limit limit);

        [OperationContract]
        void UpdateLimit(Models.Limit limit);

        [OperationContract]
        void DeleteLimit(string limitId);

        [OperationContract]
        List<Models.Credential> GetCredentials();

        [OperationContract]
        Models.Credential GetCredential(string CredentialId);

        [OperationContract]
        void CreateCredential(Models.Credential credential);

        [OperationContract]
        void UpdateCredential(Models.Credential credential);

        [OperationContract]
        void DeleteCredential(string credentialId);

    }

    // Utilisez un contrat de données (comme illustré dans l'exemple ci-dessous) pour ajouter des types composites aux opérations de service.
    // Vous pouvez ajouter des fichiers XSD au projet. Une fois le projet généré, vous pouvez utiliser directement les types de données qui y sont définis, avec l'espace de noms "BudGestWcfService.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
