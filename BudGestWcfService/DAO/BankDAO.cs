﻿using System.Collections.Generic;
using BankPostgreContext;
using System.Linq;
namespace BudGestWcfService.DAO
{
    public class BankDAO 
    {
        BankPostgreDataContext bdc;
        public BankDAO() 
        {
            bdc = new  BankPostgreDataContext();
        }

        #region ICrud<Bank> Membres

        public void Create(Bank addObject)
        {
            bdc.Banks.InsertOnSubmit(addObject);
            bdc.SubmitChanges();
        }

        public Bank Get(int id)
        {
            return bdc.Banks.Where(b => b.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Update Bank,
        /// you have to get Bank with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Bank updateObject)
        {
            Bank upd = this.Get(updateObject.Id);
            upd.Address = updateObject.Address;
            //upd.Bankaccounts = updateObject.Bankaccounts;
            //upd.Name = updateObject.Name;
            //upd.
            //upd.Parentcategoryid = updateObject.Parentcategoryid;
            bdc.SubmitChanges();
        }


        public void Delete(int id)
        {
            Bank del = this.Get(id);
            bdc.Banks.DeleteOnSubmit(del);
            bdc.SubmitChanges();
        }

        #endregion

        public List<Bank> GetBanks()
        {
            return bdc.Banks.ToList<Bank>();
        }

        public Bank GetBank(string name)
        {
            return bdc.Banks.Where(b => b.Name == name).FirstOrDefault();
        }

        public List<Bank> SearchBank(string search)
        {
            return bdc.Banks.Where(b => b.Name.Contains(search)).ToList<Bank>();
        }
    }
}