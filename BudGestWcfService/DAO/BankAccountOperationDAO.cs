﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankPostgreContext;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class BankAccountOperationDAO 
    {

        BankPostgreDataContext baodc;

        public BankAccountOperationDAO()
        {
            baodc = new BankPostgreDataContext();
        }
        #region ICrud<Bankaccountoperation> Membres

        public void Create(Bankaccountoperation addObject)
        {
            baodc.Bankaccountoperations.InsertOnSubmit(addObject);
            baodc.SubmitChanges();
        }
        /// <summary>
        /// Update BankAccountOperation,
        /// you have to get BankAccountOperation with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Bankaccountoperation updateObject)
        {
            baodc.SubmitChanges();
        }


        #endregion

        public List<Bankaccountoperation> GetByBankAccountId(int id)
        {
            return baodc.Bankaccountoperations.Where(bao => bao.Bankaccountid == id).ToList();
        }

        public List<Operation> GetOperationByBankAccountId(int id)
        {
            BudGestPostgreDataContext odc = new BudGestPostgreDataContext();
            return odc.Operations.Where(x => x.Bankaccountid == id).ToList();
            //return new List<Operation>();
        }

        public void DeleteBankAccountOperation(int bankaccountid, int operationid)
        {
            Bankaccountoperation del  = baodc.Bankaccountoperations.Where(bao=>bao.Bankaccountid==bankaccountid && bao.Operationid==operationid).FirstOrDefault();
            if (del != null)
            {
                baodc.Bankaccountoperations.DeleteOnSubmit(del);
                baodc.SubmitChanges();
            }
            
        }
    }
}