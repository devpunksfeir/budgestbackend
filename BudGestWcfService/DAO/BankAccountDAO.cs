﻿using System;
using System.Collections.Generic;
using BankPostgreContext;
using System.Linq;

namespace BudGestWcfService.DAO
{
    public class BankAccountDAO 
    {
        BankPostgreDataContext badc;

        public BankAccountDAO()
        {
            badc = new BankPostgreDataContext();
        }
        public List<Bankaccount> GetByUser(int userId)
        {
            return badc.Bankaccounts.Where(ba => ba.Userid == userId).ToList<Bankaccount>();
        }

        #region ICrud<Bankaccount> Membres

        public void Create(Bankaccount addObject)
        {
            badc.Bankaccounts.InsertOnSubmit(addObject);
            badc.SubmitChanges();
        }

        public Bankaccount Get(int id)
        {
            return badc.Bankaccounts.Where(ba => ba.Id == id).FirstOrDefault();
        }
        /// <summary>
        /// Update BankAccount,
        /// you have to get BankAccount with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Bankaccount updateObject)
        {
            badc.SubmitChanges();
        }

        public void Delete(int id)
        {

            Bankaccount del = this.Get(id);
            badc.Bankaccounts.DeleteOnSubmit(del);
            badc.SubmitChanges();
        }

        public List<Bankaccount> GetBankAccounts()
        {
            return badc.Bankaccounts.ToList<Bankaccount>();
        }

        #endregion
    }

}