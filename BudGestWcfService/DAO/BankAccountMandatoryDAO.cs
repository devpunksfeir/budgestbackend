﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankPostgreContext;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class BankAccountMandatoryDAO
    {

        BankPostgreDataContext bamdc;

        public BankAccountMandatoryDAO()
        {
            bamdc = new BankPostgreDataContext();
        }

        #region ICrud<Bankaccountmandatary> Membres

        public void Create(Bankaccountmandatary addObject)
        {
            bamdc.Bankaccountmandataries.InsertOnSubmit(addObject);
            bamdc.SubmitChanges();
        }

        /// <summary>
        /// Update BankAccountMandatary,
        /// you have to get BankAccountMandatary with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Bankaccountmandatary updateObject)
        {
            bamdc.SubmitChanges();
        }


        #endregion

        public List<Bankaccountmandatary> GetByBankAccoundId(int id)
        {
            return bamdc.Bankaccountmandataries.Where(bam => bam.Bankaccountid == id).ToList();
        }

        public List<User> GetMandatoryByBankAccountId(int id)
        {
            BudGestPostgreDataContext mdc = new BudGestPostgreDataContext();
            //return (from bam in bamdc.Bankaccountmandataries from u in mdc.Users where bam.Bankaccountid == id && bam.Mandataryid == u.Id select u).ToList();
            return new List<User>();
        }

        public void DeleteBankAccountMandatory(int banckaccountid, int mandatoryid)
        {
            Bankaccountmandatary del = bamdc.Bankaccountmandataries.Where(bam => bam.Bankaccountid == banckaccountid && bam.Mandataryid == mandatoryid).FirstOrDefault();
            bamdc.Bankaccountmandataries.DeleteOnSubmit(del);
            bamdc.SubmitChanges();
        }
    }
}