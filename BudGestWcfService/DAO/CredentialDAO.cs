﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class CredentialDAO 
    {
        BudGestPostgreDataContext cdc;

        public CredentialDAO()
        {
            cdc = new BudGestPostgreDataContext();
        }

        #region ICrud<Credential> Membres

        public void Create(Credential addObject)
        {
            cdc.Credentials.InsertOnSubmit(addObject);
            cdc.SubmitChanges();
        }

        public Credential Get(int id)
        {
            return cdc.Credentials.Where(c => c.Id == id).FirstOrDefault();
        }

        // <summary>
        /// Update Category,
        /// you have to get Category with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Credential updateObject)
        {
            cdc.SubmitChanges();
        }

        public void Delete(int id)
        {
            Credential del = this.Get(id);
            cdc.Credentials.DeleteOnSubmit(del);
            cdc.SubmitChanges();
        }

        public List<Credential> GetCredentials()
        {
            return cdc.Credentials.ToList<Credential>();
        }

        #endregion
    }
}