﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;
namespace BudGestWcfService.DAO
{
    public class LimitDAO 
    {

        BudGestPostgreDataContext ldc;

        public LimitDAO()
        {
            ldc = new BudGestPostgreDataContext();
        }

        #region ICrud<Limite> Membres

        public void Create(Limit addObject)
        {
            ldc.Limits.InsertOnSubmit(addObject);
        }

        public Limit Get(int id)
        {
            return ldc.Limits.Where(l => l.Id == id).FirstOrDefault();
        }

        // <summary>
        /// Update Limit,
        /// you have to get Limit with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Limit updateObject)
        {
            ldc.SubmitChanges();
        }

        public void Delete(int id)
        {
            Limit del = this.Get(id);
            ldc.Limits.DeleteOnSubmit(del);
            ldc.SubmitChanges();
        }

        public List<Limit> GetLimits()
        {
            return ldc.Limits.ToList<Limit>();
        }

        #endregion
    }
}