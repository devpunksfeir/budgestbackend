﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class UserDAO 
    {

        BudGestPostgreDataContext udc;

        public UserDAO()
        {
            udc = new BudGestPostgreDataContext();
        }

        #region ICrud<User> Membres

        public void Create(User addObject)
        {
            udc.Users.InsertOnSubmit(addObject);
            udc.SubmitChanges();
        }

        public User Get(int id)
        {
            return udc.Users.Where(u => u.Id == id).FirstOrDefault();
        }
        // <summary>
        /// Update User,
        /// you have to get User with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(User updateObject)
        {
            udc.SubmitChanges();
        }

        public void Delete(int id)
        {
            User del = this.Get(id);
            udc.Users.DeleteOnSubmit(del);
            udc.SubmitChanges();
        }

        public List<User> GetUsers()
        {
            return udc.Users.ToList<User>();
        }

        #endregion
    }
}