﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;
using BankPostgreContext;

namespace BudGestWcfService.DAO
{
    public class ProfileAccountDAO 
    {
        #region ICrud<Profileaccount> Membres

        BudGestPostgreDataContext padc;

        public ProfileAccountDAO()
        {
            padc = new BudGestPostgreDataContext();
        }

        public void Create(Profileaccount addObject)
        {
            if (padc.Profileaccounts.Count() == 0) {
                addObject.Id = 0;
            }
            else
            {
                addObject.Id = padc.Profileaccounts.Max(x => x.Id) + 1;
            }
            padc.Profileaccounts.InsertOnSubmit(addObject);
            padc.SubmitChanges();
        }

        // <summary>
        /// Update ProfileAccount,
        /// you have to get ProfileAccount with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Profileaccount updateObject)
        {
            padc.SubmitChanges();
        }

        public void Delete(int bankAccountId)
        {
            var tmp = padc.Profileaccounts.Where(x => x.Bankaccountid == bankAccountId);
            padc.Profileaccounts.DeleteAllOnSubmit(tmp);
            padc.SubmitChanges();
        }

        #endregion

        public List<Bankaccount> GetBankAccountsByProfileId(int id)
        {
            List<int?> pa = padc.Profileaccounts.Where(pr => pr.Profileid == id).Select(p => p.Bankaccountid).ToList();

            BankPostgreDataContext badc = new BankPostgreDataContext();

            return badc.Bankaccounts.Where(ba => pa.Contains(ba.Id)).ToList();
        }

    }
}