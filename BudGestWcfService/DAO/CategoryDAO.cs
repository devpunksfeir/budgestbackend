﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class CategoryDAO 
    {
        BudGestPostgreDataContext cdc;

        public CategoryDAO()
        {
            cdc = new BudGestPostgreDataContext();
        }

        #region ICrud<Category> Membres

        public void Create(Category addObject)
        {
            cdc.Categories.InsertOnSubmit(addObject);
            cdc.SubmitChanges();
        }

        public Category Get(int id)
        {
            return cdc.Categories.Where(c => c.Id == id).FirstOrDefault();
        }
        /// <summary>
        /// Update Category,
        /// you have to get Category with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Category updateObject)
        {
            Category upd = this.Get(updateObject.Id);
            upd.Name = updateObject.Name;
            upd.Parentcategoryid = updateObject.Parentcategoryid;
            cdc.SubmitChanges();
        }

        public void Delete(int id)
        {
            Category del = this.Get(id);
            cdc.Categories.DeleteOnSubmit(del);
            cdc.SubmitChanges();
        }

        public void DeleteAllCategories()
        {
            cdc.Categories.DeleteAllOnSubmit(cdc.Categories);
            //Category del = this.Get(id);
            //cdc.Categories.DeleteOnSubmit(del);
            cdc.SubmitChanges();
        }


        #endregion

        public List<string> GetAllCategories()
        {
            return cdc.Categories.Select(c => c.Name).ToList();
        }

        public List<Category> GetCategories()
        {
            return cdc.Categories.ToList<Category>();
        }

        public Category GetCategoryByName(string name)
        {
            return cdc.Categories.FirstOrDefault(c => c.Name == name);
        }
    }
}