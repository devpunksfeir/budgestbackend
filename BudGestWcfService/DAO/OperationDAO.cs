﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class OperationDAO 
    {

        BudGestPostgreDataContext odc;

        public OperationDAO()
        {
            odc = new BudGestPostgreDataContext();
        }

        #region ICrud<Operation> Membres

        public void Create(Operation addObject)
        {
            odc.Operations.InsertOnSubmit(addObject);
            odc.SubmitChanges();
        }

        public Operation Get(int id)
        {
            return odc.Operations.Where(o => o.Id == id).FirstOrDefault();
        }

        // <summary>
        /// Update Operation,
        /// you have to get Operation with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Operation updateObject)
        {
            odc.SubmitChanges();
        }

        public void Delete(int id)
        {
            Operation del = this.Get(id);
            odc.Operations.DeleteOnSubmit(del);
            odc.SubmitChanges();
        }
        
        public List<Operation> GetOperations()
        {
            return odc.Operations.ToList<Operation>();
        }

        public void DeleteAllOperations()
        {
            odc.Operations.DeleteAllOnSubmit(odc.Operations);
            //Category del = this.Get(id);
            //cdc.Categories.DeleteOnSubmit(del);
            odc.SubmitChanges();
        }

        #endregion

    }
}