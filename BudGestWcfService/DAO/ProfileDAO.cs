﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class ProfileDAO 
    {
        BudGestPostgreDataContext pdc;

        public ProfileDAO()
        {
            pdc = new BudGestPostgreDataContext();
        }

        #region ICrud<Profile> Membres

        public void Create(Profile addObject)
        {
            pdc.Profiles.InsertOnSubmit(addObject);
            pdc.SubmitChanges();
        }

        public Profile Get(int id)
        {
            return pdc.Profiles.Where(p => p.Id == id).FirstOrDefault();
        }
        // <summary>
        /// Update Profile,
        /// you have to get Profile with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Profile updateObject)
        {
            var pro = this.Get(updateObject.Id);
            if (pro != null)
            {
                pro.Nickname = updateObject.Nickname;
                pro.User.Lastname = updateObject.User.Lastname;
                pro.User.Firstname = updateObject.User.Firstname;
                pro.Credential.Login = updateObject.Credential.Login;
            }
            pdc.SubmitChanges();
        }

        public void Delete(int id)
        {
            Profile del = this.Get(id);
            pdc.Profiles.DeleteOnSubmit(del);
            pdc.SubmitChanges();
        }

        #endregion

        public Profile GetProfileByNickname(string nickname)
        {
            return pdc.Profiles.Where(p => p.Nickname == nickname).FirstOrDefault();
        }

        public List<Profile> GetProfiles()
        {
            return pdc.Profiles.ToList<Profile>();
        }

    }
}