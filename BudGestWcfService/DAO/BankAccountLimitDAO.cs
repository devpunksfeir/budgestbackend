﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BankPostgreContext;
using BudGestPostgreContext;

namespace BudGestWcfService.DAO
{
    public class BankAccountLimitDAO 
    {

        BankPostgreDataContext baldc;
        public BankAccountLimitDAO()
        {
            baldc = new BankPostgreDataContext();
        }
        #region ICrud<Bankaccountlimit> Membres

        public void Create(Bankaccountlimit addObject)
        {
            baldc.Bankaccountlimits.InsertOnSubmit(addObject);
            baldc.SubmitChanges();
        }

        /// <summary>
        /// Update BankAccountLimit,
        /// you have to get BankAccountLimit with Get method, apply change, and after update
        /// </summary>
        /// <param name="updateObject"></param>
        public void Update(Bankaccountlimit updateObject)
        {
            baldc.SubmitChanges();
        }

        #endregion

        public List<Bankaccountlimit> GetByBankAccoundId(int id)
        {
            return baldc.Bankaccountlimits.Where(bal => bal.Bankaccountid == id).ToList<Bankaccountlimit>();
        }

        public List<Limit> GetLimitByBankAccoundId(int bankaccountid)
        {
            List<Limit> res = new List<Limit>();
            
            BudGestPostgreDataContext ldc = new BudGestPostgreDataContext();

            List<int?> liste = new List<int?>();
            liste = (from bal in baldc.Bankaccountlimits where bal.Limitid.HasValue select bal.Limitid).ToList();

            foreach (int i in liste)
            {
                var v = (from li in ldc.Limits where li.Id == i select li).FirstOrDefault();
                res.Add(v);
            }

            //  return (from li in ldc.Limits where bal.Bankaccountid == bankaccountid && bal.Limitid == l.Id select l).ToList<Limit>();
            return res;
        }

        public void DeleteBankAccountLimit(int bankaccountid, int limitid)
        {
            Bankaccountlimit del = baldc.Bankaccountlimits.Where(bal => bal.Bankaccountid == bankaccountid && bal.Limitid == limitid).FirstOrDefault();
            baldc.Bankaccountlimits.DeleteOnSubmit(del);
            baldc.SubmitChanges();
        }
    }
}