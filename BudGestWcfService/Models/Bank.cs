﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BudGestWcfService.Models
{
    [DataContract]
    public class Bank
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Bic { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public Credential LoginInfo { get; set; }

    }
}