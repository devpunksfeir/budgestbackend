﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudGestWcfService.Models
{
    [System.Runtime.Serialization.DataContract]
    public class Credential
    {
        [System.Runtime.Serialization.DataMember]
        public int id { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Login { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string Password { get; set; }
        [System.Runtime.Serialization.DataMember]
        public string PwdKey { get; set; }
    }
}