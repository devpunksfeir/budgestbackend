﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BudGestWcfService.Models
{
    [DataContract]
    public class BankAccount
    {

        [DataMember]
        public int profileId { get; set; }
        [DataMember]
        public User user { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public Bank bank { get; set; }
        [DataMember]
        public string devise { get; set; }
        [DataMember]
        public string iban { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public double amount { get; set; }
        [DataMember]
        public double overdraft { get; set; }
        [DataMember]
        public double ease { get; set; }
        [DataMember]
        public double earnings { get; set; }
        [DataMember]
        public List<User> mandatary { get; set; }
        [DataMember]
        public List<Limit> limits { get; set; }
        [DataMember]
        public List<Operation> operations { get; set; }
    }
}