﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudGestWcfService.Models
{
    [System.Runtime.Serialization.DataContract]
    public class Limit
    {
        [System.Runtime.Serialization.DataMember]
        public int id { get; set; }

        [System.Runtime.Serialization.DataMember]
        public double amount { get; set; }

        [System.Runtime.Serialization.DataMember]
        public string period { get; set; }
    }
}