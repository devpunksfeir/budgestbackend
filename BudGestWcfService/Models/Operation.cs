﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BudGestWcfService.Models
{
    [DataContract]
    public class Operation
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public int bankAccountId { get; set; }
        [DataMember]
        public string operationName { get; set; }
        [DataMember]
        public string operationDate { get; set; }
        [DataMember]
        public string recipient { get; set; }
        [IgnoreDataMember]
        public DateTime opeDate {
            get { return String.IsNullOrWhiteSpace(operationDate) ? new DateTime() : DateTime.Parse(operationDate) ;}
            set { opeDate = value ;} 
        }
        [DataMember]
        public double amount { get; set; }
        [DataMember]
        public Category category { get; set; }
    }
}