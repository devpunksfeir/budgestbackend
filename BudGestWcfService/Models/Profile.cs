﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BudGestWcfService.Models{
    [DataContract]
    public class Profile
    {
        [DataMember]
        public byte[] picture { get; set; }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public User user { get; set; }
        [DataMember]
        public string Nickname { get; set; }
        [DataMember]
        public Credential LoginInfo { get; set; }
        [DataMember]
        public List<BankAccount> accounts { get; set; }
    }
}