﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BudGestWcfService.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Firstname { get; set; }
        //[DataMember]
        //public DateTime birthday { get; set; }

    }
}