﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace BudGestWcfService
{
    public class BudGestRestService : IBudGestRestService
    {
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        #region Test

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdata/{value}")]
        public string GetData(string value)
        {
            Console.WriteLine("Call getdata with value " + value);
            return string.Format("You entered: {0}", value);
        }

        #endregion

        #region Bank

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbanks")]
        public List<Models.Bank> GetBanks()
        {
            return Services.BankService.GetBanks();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbank/{name}")]
        public Models.Bank GetBank(string name)
        {
            return Services.BankService.GetBank(name);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createbank")]
        public void CreateBank(Models.Bank bank)
        {
            Services.BankService.CreateBank(bank);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updatebank")]
        public void UpdateBank(Models.Bank bank)
        {
            Services.BankService.UpdateBank(bank);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                  UriTemplate = "deletebank")]
        public void DeleteBank(string bankId)
        {
            Services.BankService.DeleteBank(bankId);
        }

        #endregion

        #region Profile

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updatepassword")]
        public void UpdatePassword(Models.Credential credential)
        {
            Services.ProfileService.UpdatePassword(credential);
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprofiles")]
        public List<Models.Profile> GetProfiles()
        {
            return Services.ProfileService.GetProfiles();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprofile/{nickname}")]
        public Models.Profile GetProfile(string nickname)
        {
            return Services.ProfileService.GetProfile(nickname);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createprofile")]
        public Models.Profile CreateProfile(Models.Profile profile)
        {
            return Services.ProfileService.CreateProfile(profile);
            
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updateprofile")]
        public void UpdateProfile(Models.Profile profile)
        {
            Services.ProfileService.UpdateProfile(profile);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "deleteprofile")]
        public void DeleteProfile(string profileId)
        {
            Services.ProfileService.DeleteProfile(profileId);
        }

        #endregion

        #region Account

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbankaccounts", BodyStyle=WebMessageBodyStyle.Bare)]
        public List<Models.BankAccount> GetBankAccounts()
        {
            return Services.BankAccountService.GetBankAccounts();

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbankaccount/{bankAccountId}")]
        public Models.BankAccount GetBankAccount(string bankAccountId)
        {
            return Services.BankAccountService.GetBankAccount(bankAccountId);
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserbankaccounts/{userId}")]
        public List<Models.BankAccount> GetBankAccountByUser(string userId)
        {
            return Services.BankAccountService.GetBankAccountByUser(userId);    
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createbankaccount")]
        public int CreateBankAccount(Models.BankAccount bankAccount)
        {

            return Services.BankAccountService.CreateBankAccount(bankAccount);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updatebankaccount")]
        public void UpdateBankAccount(Models.BankAccount bankAccount)
        {
            Services.BankAccountService.UpdateBankAccount(bankAccount);    
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletebankaccount/{bankAccountId}")]
        public void DeleteBankAccount(string bankAccountId)
        {

            Services.BankAccountService.DeleteBankAccount(bankAccountId);
        }

        #endregion

        #region Operation

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getoperations")]
        public List<Models.Operation> GetOperations()
        {
            return Services.OperationService.GetOperations();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getoperation/{operationId}")]
        public Models.Operation GetOperation(string operationId)
        {
            return Services.OperationService.GetOperation(operationId);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createoperation")]
        public int CreateOperation(Models.Operation operation)
        {
            return Services.OperationService.CreateOperation(operation);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updateoperation")]
        public void UpdateOperation(Models.Operation operation)
        {
            Services.OperationService.UpdateOperation(operation);
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deleteoperation/{operationId}")]
        public void DeleteOperation(string operationId)
        {
            Services.OperationService.DeleteOperation(operationId);
        }

        #endregion

        #region Category

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategories")]
        public List<Models.Category> GetCategories()
        {
            return Services.CategoryService.GetCategories();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcategory/{categoryId}")]
        public Models.Category GetCategory(string categoryId)
        {
            return Services.CategoryService.GetCategory(categoryId);  
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createcategory")]
        public void CreateCategory(Models.Category category)
        {
            Services.CategoryService.CreateCategory(category);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updatecategory")]
        public void UpdateCategory(Models.Category category)
        {
            Services.CategoryService.UpdateCategory(category);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "deletecategory")]
        public void DeleteCategory(string categoryId)
        {
            Services.CategoryService.DeleteCategory(categoryId);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "deletecategoriesoperations")]
        public void DeleteCategoriesOperations()
        {
            Services.CategoryService.DeleteCategoriesOperations();
        }

        #endregion

        #region User

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getUsers")]
        public List<Models.User> GetUsers()
        {
            return Services.UserService.GetUsers();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuser/{userId}")]
        public Models.User GetUser(string userId)
        {
            return Services.UserService.GetUser(userId);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createuser")]
        public void CreateUser(Models.User user)
        {
            Services.UserService.CreateUser(user);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updateuser")]
        public void UpdateUser(Models.User user)
        {
            Services.UserService.UpdateUser(user);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "deleteuser")]
        public void DeleteUser(string userId)
        {
            Services.UserService.DeleteUser(userId);
        }

        #endregion

        #region Limit

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlimits")]
        public List<Models.Limit> GetLimits()
        {
            return Services.LimitService.GetLimits();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlimit/{limitId}")]
        public Models.Limit GetLimit(string limitId)
        {
            return Services.LimitService.GetLimit(limitId);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createlimit")]
        public void CreateLimit(Models.Limit limit)
        {
            Services.LimitService.CreateLimit(limit);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updatelimit")]
        public void UpdateLimit(Models.Limit limit)
        {
            Services.LimitService.UpdateLimit(limit);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "deletelimit")]
        public void DeleteLimit(string limitId)
        {
            Services.LimitService.DeleteLimit(limitId);
        }

        #endregion

        #region Credential

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcredentials")]
        public List<Models.Credential> GetCredentials()
        {
            return Services.CredentialService.GetCredentials();
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcredential/{credentialId}")]
        public Models.Credential GetCredential(string credentialId)
        {
            return Services.CredentialService.GetCredential(credentialId);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "createcredential", BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public void CreateCredential(Models.Credential credential)
        {
            Services.CredentialService.CreateCredential(credential);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "updatecredential")]
        public void UpdateCredential(Models.Credential credential)
        {
            Services.CredentialService.UpdateCredential(credential);
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
                   UriTemplate = "deletecredential")]
        public void DeleteCredential(string credentialId)
        {
            Services.CredentialService.DeleteCredential(credentialId);
        }

        #endregion

    }
}
