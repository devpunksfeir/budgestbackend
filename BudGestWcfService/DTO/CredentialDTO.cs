﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;

namespace BudGestWcfService.DTO
{
    public class CredentialDTO
    {

        public static Credential ToModelCredential(BudGestPostgreContext.Credential credential)
        {
            return new Credential()
            {
                id = credential.Id,
                Login = credential.Login,
                Password = credential.Password,
                PwdKey = credential.Pwdkey
            };
        }

        public static BudGestPostgreContext.Credential ToDBCredential(Credential credential)
        {
            return new BudGestPostgreContext.Credential()
            {
                Id = credential.id,
                Login = credential.Login,
                Password = credential.Password,
                Pwdkey = credential.PwdKey
            };
        }

    }
}