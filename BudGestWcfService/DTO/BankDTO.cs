﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;
using BudGestWcfService.DAO;

namespace BudGestWcfService.DTO
{
    
    public class BankDTO
    {

        public static Bank ToModelBank(BankPostgreContext.Bank bank)
        {
            CredentialDAO c = new CredentialDAO();
            return new Bank()
            {
                id = bank.Id,
                Name = bank.Name,
                LoginInfo = (bank.Credentialid.HasValue? CredentialDTO.ToModelCredential( c.Get(bank.Credentialid.Value)):null),
                Bic = bank.Bic,
                Address = bank.Address
            };
        }

        public static BankPostgreContext.Bank ToDBBank(Bank bank)
        {
            return new BankPostgreContext.Bank() 
            {
                Id = bank.id,
                Bic=bank.Bic,
                Address = bank.Address,
                Name = bank.Name,
                //Credentialid = bank.LoginInfo.id
            };

        }

    }
}