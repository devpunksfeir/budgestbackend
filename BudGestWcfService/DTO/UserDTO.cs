﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;

namespace BudGestWcfService.DTO
{
    public class UserDTO
    {
        public static User ToModelUser(BudGestPostgreContext.User user)
        {
            return new User()
            {
                id = user.Id,
                LastName = user.Lastname,
                Firstname = user.Firstname,
                //birthday = (user.Birthday.HasValue ? user.Birthday.Value : DateTime.Today)
            };
        }

        public static BudGestPostgreContext.User ToDBUser(User user)
        {
            return new BudGestPostgreContext.User()
            {
                Id = user.id,
                Lastname = user.LastName,
                Firstname = user.Firstname,
                //Birthday = user.birthday,
                
            };
        }
    }
}