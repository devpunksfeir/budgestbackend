﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;
using BudGestWcfService.DAO;

namespace BudGestWcfService.DTO
{
    public class OperationDTO
    {
        public static Operation ToModelOperation(BudGestPostgreContext.Operation operation)
        {
            BankAccountDAO ba = new BankAccountDAO();
            return new Operation()
            {
                id = operation.Id,
                operationName = operation.Operationname,
                operationDate = (operation.Operationdate.HasValue ? operation.Operationdate.Value.ToString("yyyy/MM/dd hh:mm:ss") : ""),
                //opeDate =(operation.Operationdate.HasValue ? operation.Operationdate.Value : new DateTime()),
                recipient = operation.Recipient,
                amount = (operation.Amount.HasValue ? operation.Amount.Value : 0.00),
                bankAccountId = (operation.Bankaccountid.HasValue ? operation.Bankaccountid.Value : -1),
                category = (operation.Category != null ?  CategoryDTO.ToModelCategory(operation.Category) : new Category()),
            };
        }

        public static BudGestPostgreContext.Operation ToDBOperation(Operation operation)
        {
            CategoryDAO ca = new CategoryDAO(); 

            return new BudGestPostgreContext.Operation()
            {
                Id = operation.id,
                Amount = operation.amount,
                Categoryid = operation.category.id,
                Bankaccountid = operation.bankAccountId,
                Recipient = operation.recipient,
                Operationname = operation.operationName,
                Operationdate = DateTime.Parse(operation.operationDate)
               //Category = ca.Get(operation.category.id)
                //Category = CategoryDTO.ToDBCategory(operation.category)
            };
        }

    }
}