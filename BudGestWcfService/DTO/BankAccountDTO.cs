﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;
using BudGestWcfService.DAO;

namespace BudGestWcfService.DTO
{
    public class BankAccountDTO
    {

        public static BankAccount ToModelBankAccount(BankPostgreContext.Bankaccount bankaccount)
        {
            BankAccountLimitDAO bal = new BankAccountLimitDAO();
            BankAccountMandatoryDAO bam = new BankAccountMandatoryDAO();
            BankAccountOperationDAO bao = new BankAccountOperationDAO();
            UserDAO u = new UserDAO();
            //return new BankAccount()
            //{
            //    amount = (bankaccount.Amount.HasValue ? bankaccount.Amount.Value : 0.00),
            //    bank = BankDTO.ToModelBank(bankaccount.Bank),
            //    devise = bankaccount.Devise,
            //    earnings = (bankaccount.Earnings.HasValue ? bankaccount.Earnings.Value : 0.00),
            //    ease = (bankaccount.Ease.HasValue ? bankaccount.Ease.Value : 0.00),
            //    iban = bankaccount.Iban,
            //    id = bankaccount.Id,
            //    limits = bal.GetLimitByBankAccoundId(bankaccount.Id).Select(l => LimitDTO.ToModelLimit(l)).ToList(),
            //    mandatary = bam.GetMandatoryByBankAccountId(bankaccount.Id).Select(m => UserDTO.ToModelUser(m)).ToList(),
            //    name = bankaccount.Name,
            //    operations = bao.GetOperationByBankAccountId(bankaccount.Id).Select(o => OperationDTO.ToModelOperation(o)).ToList(),
            //    overdraft = (bankaccount.Overdraft.HasValue ? bankaccount.Overdraft.Value : 0.00),
            //    type = bankaccount.Type,
            //    user = (bankaccount.Userid.HasValue ? UserDTO.ToModelUser(u.Get(bankaccount.Userid.Value)) : null)
            //};

            BankAccount b = new BankAccount()             
            {
                amount = (bankaccount.Amount.HasValue ? bankaccount.Amount.Value : 0.00),
                bank = BankDTO.ToModelBank(bankaccount.Bank),
                devise = bankaccount.Devise,
                earnings = (bankaccount.Earnings.HasValue ? bankaccount.Earnings.Value : 0.00),
                ease = (bankaccount.Ease.HasValue ? bankaccount.Ease.Value : 0.00),
                iban = bankaccount.Iban,
                id = bankaccount.Id,
                limits = bal.GetLimitByBankAccoundId(bankaccount.Id).Select(l => LimitDTO.ToModelLimit(l)).ToList(),
                mandatary = bam.GetMandatoryByBankAccountId(bankaccount.Id).Select(m => UserDTO.ToModelUser(m)).ToList(),
                name = bankaccount.Name,
                operations = bao.GetOperationByBankAccountId(bankaccount.Id).Select(o => OperationDTO.ToModelOperation(o)).ToList(),
                overdraft = (bankaccount.Overdraft.HasValue ? bankaccount.Overdraft.Value : 0.00),
                type = bankaccount.Type,
                user = (bankaccount.Userid.HasValue ? UserDTO.ToModelUser(u.Get(bankaccount.Userid.Value)) : null)
            };

            return b;
        }

        public static BankPostgreContext.Bankaccount ToDBBankAccount(BankAccount bankaccount)
        {
            return new BankPostgreContext.Bankaccount()
            {
                Amount = bankaccount.amount,
                //Bank = BankDTO.ToDBBank(bankaccount.bank),
                Bankid= bankaccount.bank.id,
                Devise= bankaccount.devise,
                Earnings= bankaccount.earnings,
                Ease=bankaccount.ease,
                Iban = bankaccount.iban,
                Id = bankaccount.id,
                Name = bankaccount.name,
                Overdraft = bankaccount.overdraft,
                Type = bankaccount.type,
                Userid = bankaccount.user.id
                
            };
        }
    }
}