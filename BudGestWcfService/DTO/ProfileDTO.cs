﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;
using BudGestWcfService.DAO;

namespace BudGestWcfService.DTO
{
    public class ProfileDTO
    {
        public static Profile ToModelProfile(BudGestPostgreContext.Profile profile)
        {
            
            ProfileAccountDAO pa = new ProfileAccountDAO();
            var tmp = pa.GetBankAccountsByProfileId(profile.Id);
            return new Profile()
            {
                id=profile.Id,
                Nickname= profile.Nickname,
                picture =profile.Picture,
                LoginInfo= CredentialDTO.ToModelCredential(profile.Credential),
                user= UserDTO.ToModelUser(profile.User),
                accounts =  tmp.Select(a=> BankAccountDTO.ToModelBankAccount(a)).ToList()
            };
        }

        public static BudGestPostgreContext.Profile ToDBProfile(Profile profile)
        {
            return new BudGestPostgreContext.Profile()
            {
                Id=profile.id,
                Credentialid = profile.LoginInfo.id,
                Credential = CredentialDTO.ToDBCredential(profile.LoginInfo),
                Nickname= profile.Nickname,
                Picture = profile.picture,
                User= UserDTO.ToDBUser(profile.user),
                Userid = profile.user.id
            };
        }
    }
}