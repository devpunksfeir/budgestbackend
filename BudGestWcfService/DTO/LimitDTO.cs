﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;

namespace BudGestWcfService.DTO
{
    public class LimitDTO
    {

        public static Limit ToModelLimit(BudGestPostgreContext.Limit limit)
        {
            return new Limit()
            {
                id = limit.Id,
                amount = (limit.Amount.HasValue) ? 0.00 : limit.Amount.Value,
                period = limit.Period
            };
        }

        public static BudGestPostgreContext.Limit ToDBLimit(Limit limit)
        {
            return new BudGestPostgreContext.Limit()
            {
                Id = limit.id,
                Amount = limit.amount,
                Period = limit.period
            };
        }
    }
}