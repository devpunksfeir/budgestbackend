﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BudGestWcfService.Models;

namespace BudGestWcfService.DTO
{
    public class CategoryDTO
    {
        public static Category ToModelCategory(BudGestPostgreContext.Category category)
        {
            return new Category()
            {
                id = category.Id,
                name = category.Name,
                parentCategory = category.Parentcategoryid
            };
        }

        public static BudGestPostgreContext.Category ToDBCategory(Category category)
        {
            return new BudGestPostgreContext.Category()
            {
                Id = category.id,
                Name = category.name,
                Parentcategoryid = category.parentCategory
            };
        }
    }
}